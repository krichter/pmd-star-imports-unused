package de.richtercloud.pmd.star.imports.unsued;

import org.apache.commons.text.*;

public class SomeClass {

    public void someMethod() {
        WordUtils.capitalize("abc");
        CaseUtils.toCamelCase("abc", true);
    }
}
